module Task.Script.Example.Empty where

import Preload

import Data.HashMap as HashMap
import Task.Script.Annotation (Checked)
import Task.Script.Builder (branch, enter, execute, lift, pair, hole, cont, end, match, step, view)
import Task.Script.Context (Context, Typtext, recordOf', recordOf, taskOf, (:->))
import Task.Script.Syntax (Arguments(..), Constant(..), Expression(..), Match(..), Task)
import Task.Script.Type (BasicType(..), PrimType(..))
import Task.Script.World (World, Tasktext)

types :: Typtext
types =  
    from []

context :: Context 
context = from []

tasks :: Tasktext 
tasks = from []

world :: World 
world = { types, context, tasks } 
