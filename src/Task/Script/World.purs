module Task.Script.World where

import Preload

import Data.Argonaut (jsonEmptyObject)
import Data.Argonaut.Decode (decodeJson)
import Data.Argonaut.Decode.Class (class DecodeJson)
import Data.Argonaut.Decode.Combinators ((.:))
import Data.Argonaut.Encode (encodeJson)
import Data.Argonaut.Encode.Class (class EncodeJson)
import Data.Argonaut.Encode.Combinators ((:=), (~>))
import Data.HashMap as HashMap
import Task.Script.Annotation (Checked)
import Task.Script.Context (Context, Typtext)
import Task.Script.Label (Labeled, arrayTupleToLabeled)
import Task.Script.Syntax (Task)
import Task.Script.Type (BasicType, FullType)

type Parameters = Labeled FullType
type Tasktext = Labeled (Parameters * Checked Task)

newtype Parameters' = Parameters' Parameters

instance EncodeJson Parameters' where 
  encodeJson (Parameters' p) = "parameters" := encodeJson (HashMap.toArrayBy (\k v -> k~v) p) ~> jsonEmptyObject 

instance DecodeJson Parameters' where 
  decodeJson json = do 
    obj <- decodeJson json 
    params :: Array (String*FullType) <- obj .: "parameters"
    done <| (Parameters' (arrayTupleToLabeled params))

newtype Tasktext' = Tasktext' (Labeled (Parameters' * (Checked Task)))

instance EncodeJson Tasktext' where 
  encodeJson (Tasktext' tt) = "tasktext" := encodeJson (HashMap.toArrayBy (\k v -> k~v) tt) ~> jsonEmptyObject

 
instance DecodeJson Tasktext' where 
  decodeJson json = do 
    obj <- decodeJson json 
    tt :: Array (String * (Parameters' * (Checked Task))) <- obj .: "tasktext"
    done <| (Tasktext' (arrayTupleToLabeled tt)) 

fromParameters' :: Parameters' -> Parameters 
fromParameters' (Parameters' p) = p 

fromTasktext' :: Tasktext' -> Tasktext 
fromTasktext' (Tasktext' taskt) = map (\(p~t) -> ((fromParameters' p)~t)) taskt

toParameters' :: Parameters -> Parameters' 
toParameters' p = Parameters' p 

toTasktext' :: Tasktext -> Tasktext' 
toTasktext' taskt = Tasktext' (map (\(p~t) -> ((toParameters' p)~t)) taskt)

type World =
  { types :: Typtext
  , context :: Context
  , tasks :: Tasktext
  } 

newtype World' = World' World  --Purescript does not support making instances for row types, therefore a re-definition using newtype keyword

-- JSon instances 
instance EncodeJson World' where 
  encodeJson (World' {types: tyt, context: ct, tasks: tt }) = "typtext" := encodeJson (HashMap.toArrayBy (\k v -> k~v) tyt) 
    ~> "context" := encodeJson (HashMap.toArrayBy (\k v -> k~v) ct) 
    ~> "tasks" := encodeJson (toTasktext' tt)
    ~> jsonEmptyObject

instance DecodeJson World' where 
  decodeJson json = do 
    obj <- decodeJson json 
    ct :: Array (String * FullType) <- obj .: "context"
    tyt :: Array (String * BasicType) <- obj .: "typtext"
    taskt :: Tasktext' <- obj .: "tasks"

    done <| (World' { context: (arrayTupleToLabeled ct), 
      types: (arrayTupleToLabeled tyt), 
      tasks: (fromTasktext' taskt)})