module Task.Script.Menu where

import Preload

import Concur.Dom (Widget)
import Concur.Dom.Attr as Attr
import Concur.Dom.Input as Input
import Concur.Dom.Style as Style
import Concur.Dom.Text as Text
import Data.Argonaut.Core (stringify)
import Data.Argonaut.Encode (encodeJson)
import Data.HashMap as HashMap
import Task.Script.Annotation (Checked)
import Task.Script.Builder as Builder
import Task.Script.Label (Name)
import Task.Script.Saver (saveWidget)
import Task.Script.Syntax (Task)
import Task.Script.Type (ofBasic)
import Task.Script.World (World, Tasktext, Parameters)

---- Side menu ------------------------------------------------------
sideMenu :: Tasktext -> Widget (Name*Parameters)
sideMenu tt = 
    let 
        arr = HashMap.toArrayBy (\k (_~v) -> k~v) tt --Array Name*Checked Task
        items = map (\(n~_) -> sideMenuItem n) arr
    in 
        Style.menuColumn items 

sideMenuItem :: Name -> Widget (Name*Parameters)
sideMenuItem n  = 
    Style.row [ 
        sideMenuButton n 
        , Text.text "---x"
        ]

--sideMenuButton: Need to save the current task under its proper (perhaps updated) name.
--Need to lookup the correct name in the Tasktext 
--Need to return the name~parameters~checked task of the next task
sideMenuButton :: Name -> Widget (Name*Parameters)
sideMenuButton n = do 
        Style.element [ Attr.onClick ->> (n~(from [])) ] [ Text.text n ]

---- Top menu -------------------------------------------------------
topMenu :: Name -> Parameters -> Checked Task -> World -> Widget (Name * World)
topMenu n ps task {types: s, context: g, tasks: tt} = 
    Style.menuRow [
        newTaskButton tt (from []) >-> (\(n'~tt') -> (n'~{types: s, context: g, tasks: tt'}))
        , saveAsButton task
        , loadButton task
        , titleEditor n ps task {types:s, context: g, tasks: tt}
        ]

titleEditor :: Name -> Parameters -> Checked Task -> World -> Widget (Name * World)
titleEditor name params task {types: s, context: g, tasks: tt} = do
    result <- Input.entry Style.Large name ""
    done <| (result ~ {types: s, context: g, tasks: (HashMap.insert result (params~task) (HashMap.delete name tt)) })
    

---- New Task Button ------------------------------------------------
newTaskButton :: Tasktext -> Parameters -> Widget (Name*Tasktext)
newTaskButton tt ps = 
    let 
        newname = "unnamed task: " ++ show (HashMap.size tt)
    in 
        do 
            Input.button Style.Default Style.Normal Style.Large "New task"
            done <| (newname~(HashMap.insert newname (ps~Builder.hole) tt))
            --HashMap.insert ("unnamed task: " ++ show (HashMap.size tt)) (ps~Builder.hole) tt ->> liftWidget ("unnamed task: " ++ show (HashMap.size tt - 1))


---- Save As Screen ----------------------------------------------------
saveAsButton :: forall a. (Checked Task) -> Widget a
saveAsButton task = do
        void (Input.button Style.Default Style.Normal Style.Large "Save As")
        --_ <- saveWidget task "test.txt" <|> saveButton task
        saveAsScreen task
        --saveButton task

saveAsScreen :: forall a. (Checked Task) -> Widget a
saveAsScreen task = do 
    --Show widget with text and OK button
    Style.saveScreenDiv [Text.text (stringify (encodeJson task))]
    

---- Load Screen ----------------------------------------------------

loadButton :: forall a. (Checked Task) -> Widget a
loadButton task = Text.text "load"
    --Input.button Style.Default Style.Normal Style.Large "Load"
    --Function call 
    

---- Complete menu --------------------------------------------------
--menu :: Widget Unit
--menu = topMenu [Text.text "Test text", Text.text "2nd Test text", Input.button Style.Action Style.Normal Style.Small "Test button"]
