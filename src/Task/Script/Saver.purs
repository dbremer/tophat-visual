module Task.Script.Saver (
    saveWidget
) where

import Preload

import Concur.Dom (Widget)
import Data.Argonaut (stringify)
import Data.Argonaut.Encode (encodeJson)
import Effect.Aff (Aff, attempt, message)
import Effect.Aff.Class (liftAff)
import Effect.Class.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Aff (writeTextFile)
import Node.Path (FilePath)
import Task.Script.Annotation (Checked)
import Task.Script.Syntax (Task)

taskToFile :: (Checked Task) -> FilePath -> Aff Unit
taskToFile task path = do 
    res <- attempt <| writeTextFile UTF8 path (stringify (encodeJson task))
    case res of 
        Left e -> log <| "There was a problem with file: " ++ path ++ ", message: " ++ message e
        _ -> done unit

saveWidget :: (Checked Task) -> FilePath -> Widget (Checked Task)
saveWidget task path = liftAff (taskToFile task path ->> task)