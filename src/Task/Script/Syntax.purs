module Task.Script.Syntax
  ( Arguments(..)
  , Branches
  , Constant(..)
  , Expression(..)
  , LabeledBranches
  , Match(..)
  , Task(..)
  )
  where

import Preload

import Data.Argonaut (jsonEmptyObject)
import Data.Argonaut.Decode (decodeJson)
import Data.Argonaut.Decode.Class (class DecodeJson)
import Data.Argonaut.Decode.Combinators ((.:), (.:?))
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode (encodeJson)
import Data.Argonaut.Encode.Class (class EncodeJson)
import Data.Argonaut.Encode.Combinators ((:=), (~>))
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.Doc (class Display, display)
import Data.Doc as Doc
import Data.Generic.Rep (class Generic)
import Data.HashMap (toArrayBy)
import Data.HashMap as HashMap
import React.DOM.Dynamic (fieldset')
import Task.Script.Context (isOperator, unreadable)
import Task.Script.Label (Label, Labeled, Name, arrayTupleToLabeled, showFields)
import Task.Script.Type (FullType)

---- Expressions ---------------------------------------------------------------

data Expression
  = Lambda Match FullType Expression
  | Apply Expression Expression
  | Variable Name
  | IfThenElse Expression Expression Expression
  | Case Expression (Labeled (Match * Expression))
  | Record (Labeled Expression)
  | Wildcard
  | Variant Label Expression FullType
  | Nil FullType
  | Cons Expression Expression
  | Constant Constant

derive instance Eq Expression

instance EncodeJson Expression where 
  encodeJson = case _ of 
    Lambda m f e -> "type" := encodeJson "lambda" ~> "match" := m ~> "ft" := f ~> "e1" := e ~> jsonEmptyObject
    Apply e1 e2 -> "type" := encodeJson "apply" ~> "e1" := e1 ~> "e2" := e2 ~> jsonEmptyObject 
    Variable n -> "type" := encodeJson "variable" ~> "name" := n ~> jsonEmptyObject 
    IfThenElse e1 e2 e3 -> "type" := encodeJson "ifthenelse" ~> "e1" := e1 ~> "e2" := e2 ~> "e3" := e3 ~> jsonEmptyObject 
    Case e lme -> "type" := encodeJson "case" ~> "e1" := e ~> "lme" := encodeJson (toArrayBy (\k (m~e) -> k~(m~e)) lme) ~> jsonEmptyObject
    Record rec -> "type" := encodeJson "record" ~> "rec" := encodeJson (toArrayBy (\k v -> k~v) rec)
    Wildcard -> "type" := encodeJson "wildcard" ~> jsonEmptyObject 
    Variant l e ft -> "type" := encodeJson "variant" ~> "label" := l ~> "e1" := e ~> "ft" := ft ~> jsonEmptyObject 
    Nil ft -> "type" := encodeJson "nil" ~> "ft" := ft ~> jsonEmptyObject
    Cons e1 e2 -> "type" := encodeJson "cons" ~> "e1" := e1 ~> "e2" := e2 ~> jsonEmptyObject 
    Constant c -> "type" := encodeJson "constant" ~> "constant" := c ~> jsonEmptyObject 


instance DecodeJson Expression where 
  decodeJson json = do 
    obj <- decodeJson json 
    t :: String <- obj .: "type"
    m :: Maybe Match <- obj .:? "match"
    ft :: Maybe FullType <- obj .:? "ft"
    e1 :: Maybe Expression <- obj .:? "e1"
    e2 :: Maybe Expression <- obj .:? "e2"
    e3 :: Maybe Expression <- obj .:? "e3"
    n  :: Maybe Name <- obj .:? "name"
    lme :: Maybe (Array (String*(Match*Expression))) <- obj .:? "lme"
    rec :: Maybe (Array (String*Expression)) <- obj .:? "rec"
    l :: Maybe Label <- obj .:? "label"
    c :: Maybe Constant <- obj .:? "constant"
    done <| (case t of 
      "lambda" -> case m, ft, e1 of 
        Just m', Just ft', Just e1' -> Lambda m' ft' e1'
        _, _, _ -> Wildcard 
      "apply" -> case e1, e2 of 
        Just e1', Just e2' -> Apply e1' e2' 
        _, _ -> Wildcard 
      "variable" -> case n of 
        Just n' -> Variable n'
        _ -> Wildcard 
      "ifthenelse" -> case e1, e2, e3 of 
        Just e1', Just e2', Just e3' -> IfThenElse e1' e2' e3'
        _, _, _ -> Wildcard  
      "case" -> case e1, lme of 
        Just e1', Just lme' -> Case e1' (arrayTupleToLabeled lme') 
        _, _ -> Wildcard 
      "record" -> case rec of 
        Just rec' -> Record (arrayTupleToLabeled rec')
        _ -> Wildcard 
      "wildcard" -> Wildcard
      "variant" -> case l, e1, ft of 
        Just l', Just e1', Just ft' -> Variant l' e1' ft' 
        _, _, _ -> Wildcard 
      "nil" -> case ft of 
        Just ft' -> Nil ft' 
        _ -> Wildcard
      "cons" -> case e1, e2 of 
        Just e1', Just e2' -> Cons e1' e2'
        _, _ -> Wildcard 
      "constant" -> case c of 
        Just c' -> Constant c'
        _ -> Wildcard 
      _ -> Wildcard
    )


instance Show Expression where
  show = case _ of
    Lambda m t e -> unwords [ show m, ":", show t, ".", show e ]
    Apply (Apply (Variable n1) e2) e3 -> -- special case for operators
      if isOperator n1 then unwords [ show e2, n1, show e3 ]
      else unwords [ unreadable n1, show e2, show e3 ]
    Apply e1 e2 -> unwords [ "(", show e1, show e2, ")" ] --FIXME: parens needed?
    Variable n -> unreadable n
    IfThenElse e1 e2 e3 ->
      unlines
        [ unwords [ "if", show e1 ]
        , unwords [ "then", show e2 ] |> indent 2
        , unwords [ "else", show e3 ] |> indent 2
        ]
    Case e0 ms ->
      unlines
        [ unwords [ "case", show e0, "of" ]
        , unlines (HashMap.toArrayBy (\m e -> unwords [ show m, "|->", show e ] |> indent 2) ms)
        ]
    Record es -> showFields "=" es
    Wildcard -> "{..}"
    Variant l e t -> unwords [ l, show e, "as", show t ]
    Nil t -> unwords [ "[]", "as", show t ]
    Cons e1 e2 -> unwords [ show e1, "::", show e2 ]
    Constant c -> show c

data Arguments
  = ARecord (Labeled Expression)

derive instance Eq Arguments

instance EncodeJson Arguments where 
  encodeJson (ARecord args) = encodeJson (toArrayBy (\k v -> k~v) args)

instance Show Arguments where
  show (ARecord es) = showFields "=" es

data Constant
  = B Bool
  | I Int
  | S String

derive instance Eq Constant

derive instance Generic Constant _ 

instance EncodeJson Constant where 
  encodeJson c = genericEncodeJson c 

instance DecodeJson Constant where  
  decodeJson c = genericDecodeJson c 

--instance EncodeJson Constant where 
--  encodeJson c = encodeJson (show c)

instance Show Constant where
  show = case _ of
    B true -> "True"
    B false -> "False"
    I i -> show i
    S s -> show s

---- Matches -------------------------------------------------------------------

data Match
  = MIgnore
  | MBind Name
  | MRecord (Labeled Match)
  | MUnpack

derive instance Eq Match

instance EncodeJson Match where 
  encodeJson m = case m of 
    MIgnore -> "type" := encodeJson "mignore" ~> jsonEmptyObject
    MBind n -> "type" := encodeJson "mbind" ~> "name" := encodeJson n ~> jsonEmptyObject 
    MRecord lm -> "type" := encodeJson "mrecord" ~> "record" := encodeJson (toArrayBy (\k v -> k~v) lm) ~> jsonEmptyObject 
    MUnpack -> "type" := "munpack" ~> jsonEmptyObject 

instance DecodeJson Match where 
  decodeJson json = do 
    obj <- decodeJson json 
    t :: String <- obj .: "type"
    n :: Maybe Name <- obj .:? "name"
    lm :: Maybe (Array (String*Match)) <- obj .: "record"
    done <| (case t of 
      "mignore" -> MIgnore
      "mbind" -> case n of 
        Just n' -> MBind n' 
        _ -> MIgnore
      "mrecord" -> case lm of 
        Just lm' -> MRecord (arrayTupleToLabeled lm')
        _ -> MIgnore 
      "munpack" -> MUnpack
      _ -> MIgnore 
    )


instance Show Match where
  show = case _ of
    MIgnore -> "_"
    MBind x -> x
    MRecord ms -> showFields "≈" ms
    MUnpack -> "{..}"

---- Statements ----------------------------------------------------------------

-- data Statement t
--   = Step' Match t (Statement t)
--   | Task' t
-- derive instance Eq t => Eq (Statement t)
-- instance Show t => Show (Statement t) where
--   show = case _ of
--     Step' m t s -> unlines [ unwords [ show m, "<-", show t ], show s ]
--     Task' t -> show t

type Branches t = Array (Expression * t)

type LabeledBranches t = Array (Label * Expression * t)

-- NOTE:
-- Be aware of the INVARIANT: Branch and Select need to be inside a Step.
-- The official gramar reflects this, but the grammar defined here is flatened,
-- to parametrise it as a functor and tie the knot using custom annotations.
-- (See Task.Script.Annotation.)
data Task t
  -- Steps
  = Step Match t t
  | Branch (Branches t)
  | Select (LabeledBranches t)
  -- Editors (How do these relate to TopHat names for editors?)
  | Enter Name --Unvalued
  | Update Expression --Valued
  | Change Expression --Shared
  | View Expression --Valued read-only
  | Watch Expression --Shared read-only
  -- Combinators
  | Lift Expression
  | Pair (Array t) --"And"
  | Choose (Array t) -- "Or"
  -- Extras
  | Execute Name Arguments
  | Hole Arguments
  -- Shares
  | Share Expression
  | Assign Expression Expression

instance (EncodeJson t) => EncodeJson (Task t) where 
  encodeJson = case _ of 
    Step m t s -> "type" := encodeJson "step" ~> "match" := m ~> "t1" := t ~> "t2" := s ~> jsonEmptyObject
    Branch b -> "type" := encodeJson "branch" ~> "branches" := b ~> jsonEmptyObject 
    Select b -> "type" := encodeJson "select" ~> "lbranches" := b ~> jsonEmptyObject  
    Enter n -> "type" := encodeJson "enter" ~> "name" := n ~> jsonEmptyObject 
    Update e -> "type" := encodeJson "update" ~> "expression" := e ~> jsonEmptyObject  
    Change e -> "type" := encodeJson "change" ~> "expression" := e ~> jsonEmptyObject 
    View e -> "type" := encodeJson "view" ~> "expression" := e ~> jsonEmptyObject 
    Watch e -> "type" := encodeJson "watch" ~> "expression" := e ~> jsonEmptyObject  
    Lift e -> "type" := encodeJson "lift" ~> "expression" := e ~> jsonEmptyObject  
    Pair arr -> "type" := encodeJson "pair" ~> "arrt" := arr ~> jsonEmptyObject  
    Choose arr -> "type" := encodeJson "choose" ~> "arrt" := arr ~> jsonEmptyObject 
    Execute n a -> "type" := encodeJson "execute" ~> "name" := n ~> "args" := a ~> jsonEmptyObject
    Hole a -> "type" := encodeJson "hole" ~> "args" := a ~> jsonEmptyObject  
    Share e -> "type" := encodeJson "share" ~> "expression" := e ~> jsonEmptyObject  
    Assign e1 e2 -> "type" := encodeJson "assign" ~> "expression1" := e1 ~> "expression2" := e2 ~> jsonEmptyObject

instance (DecodeJson t) => DecodeJson (Task t) where 
  decodeJson json = do 
    obj <- decodeJson json 
    ty :: String <- obj .: "type"
    m :: Maybe Match <- obj .:? "match"
    t1 :: Maybe t <- obj .:? "t1" 
    t2 :: Maybe t <- obj .:? "t2"
    br :: Maybe (Branches t) <- obj .:? "branches"
    lbr :: Maybe (LabeledBranches t) <- obj .:? "lbranches"
    n :: Maybe Name <- obj .:? "name"
    e :: Maybe Expression <- obj .:? "expression"
    e1 :: Maybe Expression <- obj .:? "expression1"
    e2 :: Maybe Expression <- obj .:? "expression2"
    arrt :: Maybe (Array t) <- obj .:? "arrt" 
    args :: Maybe (Array (String*Expression)) <- obj .:? "args"
    done <| (case ty of 
      "step" -> case m, t1, t2 of 
        Just m', Just t1', Just t2' -> Step m' t1' t2'
        _, _, _ -> Enter "error"
      "branch" -> case br of 
        Just br' -> Branch br'
        _ -> Enter "error"
      "select" -> case lbr of 
        Just lbr' -> Select lbr' 
        _ -> Enter "error"
      "enter" -> case n of 
        Just n' -> Enter n' 
        _ -> Enter "error"
      "update" -> case e of 
        Just e' -> Update e' 
        _ -> Enter "error"
      "change" -> case e of 
        Just e' -> Change e' 
        _ -> Enter "error"
      "view" -> case e of 
        Just e' -> View e' 
        _ -> Enter "error"
      "watch" -> case e of 
        Just e' -> Watch e' 
        _ -> Enter "error"
      "lift" -> case e of 
        Just e' -> Lift e' 
        _ -> Enter "error"
      "pair" -> case arrt of 
        Just arrt' -> Pair arrt' 
        _ -> Enter "error"
      "choose" -> case arrt of 
        Just arrt' -> Choose arrt' 
        _ -> Enter "error"
      "execute" -> case n, args of 
        Just n', Just args' -> Execute n' (ARecord (arrayTupleToLabeled args'))
        _, _ -> Enter "error"
      "hole" -> case args of 
        Just args' -> Hole (ARecord (arrayTupleToLabeled args'))
        _ -> Enter "error"
      "share" -> case e of 
        Just e' -> Share e' 
        _ -> Enter "error"
      "assign" -> case e1, e2 of 
        Just e1', Just e2' -> Assign e1' e2' 
        _, _ -> Enter "error"
      _ -> Enter "error" 
    )

derive instance Eq t => Eq (Task t)

derive instance Functor Task

instance Display t => Show (Task t) where
  show = display >> Doc.render

--Pretty printer
instance Display t => Display (Task t) where
  display = case _ of
    Step m t s -> Doc.lines [ Doc.words [ Doc.show m, Doc.text "<-", display t ], display s ]
    Branch [ Constant (B true) ~ t ] -> display t --Doc.lines [ Doc.text "branch", inner' bs ]
    Branch bs -> Doc.lines [ Doc.text "branch", inner' bs ]
    Select bs -> Doc.lines [ Doc.text "select", inner'' bs ]
    Enter t -> Doc.words [ Doc.text "enter", Doc.text t ]
    Update e -> Doc.words [ Doc.text "update", Doc.show e ]
    Change e -> Doc.words [ Doc.text "change", Doc.show e ]
    View e -> Doc.words [ Doc.text "view", Doc.show e ]
    Watch e -> Doc.words [ Doc.text "watch", Doc.show e ]
    Lift e -> Doc.words [ Doc.text "done", Doc.show e ]
    Pair ss -> inner "and" ss --Doc.lines [ Doc.text "pair", inner ss ]
    Choose ss -> inner "or" ss --Doc.lines [ Doc.text "choose", inner ss ]
    Execute n as -> Doc.words [ Doc.text (unreadable n), Doc.show as ]
    Hole as -> Doc.words [ Doc.text "?", Doc.show as ]
    Share e -> Doc.words [ Doc.text "share", Doc.show e ]
    Assign e1 e2 -> Doc.words [ Doc.show e1, Doc.text ":=", Doc.show e2 ]
    where
    -- inner :: Array t -> Doc
    inner comb =
      map (\s -> Doc.lines [ Doc.text comb, Doc.indent (display s) ])
        >> Doc.lines

    inner' =
      map (\(e ~ s) -> Doc.lines [ Doc.words [ Doc.show e, Doc.text "->" ], Doc.indent (display s) ])
        >> Doc.lines
        >> Doc.indent

    inner'' =
      map (\(l ~ e ~ s) -> Doc.lines [ Doc.words [ Doc.text l, Doc.text "|", Doc.show e, Doc.text "->" ], Doc.indent (display s) ])
        >> Doc.lines
        >> Doc.indent

-- Helper functions ---------------------------------------------------
concatStrings :: Array String -> String
concatStrings = foldl (\acc s -> acc ++ s) ""

brackets :: Array String -> Array String
brackets ar = map (\s -> "(" ++ s ++ ")") ar 

toTextHelper :: Array String -> String 
toTextHelper ar = concatStrings (brackets ar)