module Task.Script.Type
  -- # Types
  ( FullType(..)
  , isFunction
  , isReference
  , isTask
  , ofRecord
  , ofVariant
  , ofReference
  , ofTask
  , PrimType(..)
  , BasicType(..)
  , ofType
  , ofBasic
  , isBasic
  ) where


import Preload

import Data.Argonaut (jsonEmptyObject)
import Data.Argonaut.Decode (decodeJson)
import Data.Argonaut.Decode.Class (class DecodeJson)
import Data.Argonaut.Decode.Combinators ((.:), (.:?))
import Data.Argonaut.Encode (encodeJson)
import Data.Argonaut.Encode.Class (class EncodeJson)
import Data.Argonaut.Encode.Combinators ((:=), (~>))
import Data.HashMap (toArrayBy)
import Task.Script.Label (Labeled, Name, arrayTupleToLabeled, showFields, showVariants)

---- Types ---------------------------------------------------------------------

data FullType
  = TFunction FullType FullType
  | TName Name
  | TList FullType
  | TRecord (Labeled FullType)
  | TVariant (Labeled FullType)
  | TReference BasicType
  | TTask (Labeled FullType)
  | TPrimitive PrimType

derive instance Eq FullType

instance EncodeJson FullType where 
  encodeJson = case _ of 
    TFunction ft1 ft2 -> "type" := encodeJson "tfunction" ~> "f1" := ft1 ~> "f2" := ft2 ~> jsonEmptyObject 
    TName name -> "type" := encodeJson "tname" ~> "name" := name ~> jsonEmptyObject 
    TList ft -> "type" := encodeJson "tlist" ~> "ft" := ft ~> jsonEmptyObject
    TRecord rec -> "type" := encodeJson "trecord" ~> "rec" := encodeJson (toArrayBy (\k v -> k~v) rec) ~> jsonEmptyObject 
    TVariant var -> "type" := encodeJson "tvariant" ~> "var" := encodeJson (toArrayBy (\k v -> k~v) var) ~> jsonEmptyObject
    TReference bt -> "type" := encodeJson "treference" ~> "bt" := bt ~> jsonEmptyObject
    TTask lft -> "type" := encodeJson "ttask" ~> "lft" := encodeJson (toArrayBy (\k v -> k~v) lft) ~> jsonEmptyObject 
    TPrimitive pt -> "type" := encodeJson "tprimitive" ~> "pt"  := encodeJson pt ~> jsonEmptyObject 

instance DecodeJson FullType where 
  decodeJson json = do 
    obj <- decodeJson json 
    t :: String <- obj .: "type" 
    f1 :: Maybe FullType <- obj .:? "f1"
    f2 :: Maybe FullType <- obj .:? "f2"
    n :: Maybe Name <- obj .:? "name"
    ft :: Maybe FullType <- obj .:? "ft"
    rec :: Maybe (Array (String*FullType)) <- obj .:? "rec"
    var :: Maybe (Array (String*FullType)) <- obj .:? "var"
    bt :: Maybe BasicType <- obj .:? "bt"
    lft :: Maybe (Array (String*FullType)) <- obj .:? "lft"
    pt :: Maybe PrimType <- obj .:? "pt"
    done <| (case t of 
      "tfunction" -> case f1, f2 of
        Just f1', Just f2' -> TFunction f1' f2' 
        _, _ -> TName "error" 
      "tname" -> case n of 
        Just n' -> TName n' 
        _ -> TName "error"
      "tlist" -> case ft of 
        Just ft' -> TList ft'
        _ -> TName "error"
      "trecord" -> case rec of 
        Just rec' -> TRecord (arrayTupleToLabeled rec')
        _ -> TName "error" 
      "tvariant" -> case var of 
        Just var' -> TVariant (arrayTupleToLabeled var') 
        _ -> TName "error"
      "treference" -> case bt of 
        Just bt' -> TReference bt' 
        _ -> TName "error" 
      "ttask" -> case lft of 
        Just lft' -> TTask (arrayTupleToLabeled lft') 
        _ -> TName "error"
      "tprimitive" -> case pt of 
        Just pt' -> TPrimitive pt'
        _ -> TName "error"
      _ -> TName "error"
    )


instance Show FullType where
  show = case _ of
    TFunction t1 t2 ->
      unlines
        [ show t1
        , indent 2 <| unwords [ "->", show t2 ]
        ]
    TName n -> n
    TList t -> unwords [ "List", show t ]
    TRecord ts -> showFields ":" ts
    TVariant ts -> showVariants ts
    TReference t -> unwords [ "Ref", show t ]
    TTask t -> unwords [ "Task", showFields ":" t ]
    TPrimitive p -> show p

isFunction :: FullType -> Bool
isFunction = case _ of
  TFunction _ _ -> true
  _ -> false

isReference :: FullType -> Bool
isReference = case _ of
  TReference _ -> true
  _ -> false

isTask :: FullType -> Bool
isTask = case _ of
  TFunction _ (TTask _) -> true
  _ -> false

ofRecord :: FullType -> Maybe (Labeled FullType)
ofRecord = case _ of
  TRecord r -> Just r
  _ -> Nothing

ofVariant :: FullType -> Maybe (Labeled FullType)
ofVariant = case _ of
  TVariant r -> Just r
  _ -> Nothing

ofReference :: FullType -> Maybe BasicType
ofReference = case _ of
  TReference b -> Just b
  _ -> Nothing

ofTask :: FullType -> Maybe (Labeled FullType)
ofTask = case _ of
  TTask r -> Just r
  _ -> Nothing

data PrimType
  = TBool
  | TInt
  | TString
  | TBuiltin Name

derive instance Eq PrimType

instance Show PrimType where
  show = case _ of
    TBool -> "Bool"
    TInt -> "Int"
    TString -> "String"
    TBuiltin n -> n

instance EncodeJson PrimType where 
  encodeJson = case _ of 
    TBool -> "type" := encodeJson "tbool" ~> jsonEmptyObject
    TInt -> "type" := encodeJson "tint" ~> jsonEmptyObject
    TString -> "type" := encodeJson "tstring" ~> jsonEmptyObject 
    TBuiltin n -> "type" := encodeJson "tbuiltin" ~> "name" := n ~> jsonEmptyObject

instance DecodeJson PrimType where 
  decodeJson json = do 
    obj <- decodeJson json 
    t :: String <- obj .: "type"
    n :: Maybe Name <- obj .:? "name"
    done <| (case t of 
      "tbool" -> TBool
      "tint" -> TInt
      "tstring" -> TString
      "tbuiltin" -> case n of 
        Just n' -> TBuiltin n'
        _ -> TBuiltin "error"
      _ -> TBuiltin "error") 

data BasicType
  = BName Name
  | BList BasicType
  | BRecord (Labeled BasicType)
  | BVariant (Labeled BasicType)
  | BPrimitive PrimType

instance EncodeJson BasicType where 
  encodeJson = case _ of 
    BName n -> "type" := encodeJson "bname" ~> "name" :=  n ~> jsonEmptyObject
    BList b -> "type" := encodeJson "blist" ~> "b" := b ~> jsonEmptyObject 
    BRecord rec -> "type" := encodeJson "brecord" ~> "record" := encodeJson (toArrayBy (\k v -> k~v) rec)
    BVariant var -> "type" := encodeJson "bvariant" ~> "variant" := encodeJson (toArrayBy (\k v -> k~v) var)
    BPrimitive prim -> "type" := encodeJson "bprimitive" ~> "primitive" := (show prim) ~> jsonEmptyObject 


--instance decodeBasicType :: DecodeJson BasicType where 
--  decodeJson json = do
--    done <| BName "Test"
-- <| maakt van BasicType een Either JsonDecodeError BasicType !!!

instance decodeBasicType :: DecodeJson BasicType where 
  decodeJson json = do 
    obj <- decodeJson json 
    t <- obj .: "type"
    n <- obj .:? "name"
    b <- obj .:? "b"
    rec :: Maybe (Array (String*BasicType)) <- obj .:? "record" 
    var :: Maybe (Array (String*BasicType))  <- obj .:? "variant"
    prim :: Maybe PrimType <- obj .:? "primitive"
    done <| (case t of
      "bname" -> case n of 
        Just s -> BName s 
        _ -> BName "error"
      "blist" -> case b of 
        Just bt -> BList bt
        _ -> BName "error"  
      "brecord" -> case rec of --rec is a Maybe (Array (String*BasicType)) here 
        Just rec' -> BRecord (arrayTupleToLabeled rec')  
        _ -> BName "error" 
      "bvariant" -> case var of 
        Just var' -> BVariant (arrayTupleToLabeled var') 
        _ -> BName "error"
      "bprimitive" -> case prim of 
        Just prim' -> BPrimitive prim'  
        _ -> BName "error"
      _ -> BName "error")

derive instance Eq BasicType

instance Show BasicType where
  show = case _ of
    BName n -> n
    BList t -> unwords [ "List", show t ]
    BRecord ts -> showFields ":" ts
    BVariant ts -> showVariants ts
    BPrimitive p -> show p

ofType :: FullType -> Maybe BasicType
ofType = case _ of
  TPrimitive p -> Just <| BPrimitive p
  TName n -> Just <| BName n
  TList t
    | Just t' <- ofType t -> Just <| BList t'
    | otherwise -> Nothing
  TRecord r
    | Just ts <- traverse ofType r -> Just <| BRecord ts
    | otherwise -> Nothing
  TVariant r
    | Just ts <- traverse ofType r -> Just <| BVariant ts
    | otherwise -> Nothing
  TFunction _ _ -> Nothing
  TReference _ -> Nothing
  TTask _ -> Nothing

ofBasic :: BasicType -> FullType
ofBasic = case _ of
  BList t -> TList <| ofBasic t
  BRecord r -> TRecord <| map ofBasic r
  BVariant r -> TVariant <| map ofBasic r
  BName n -> TName n
  BPrimitive p -> TPrimitive p

isBasic :: FullType -> Bool
isBasic t
  | Just _ <- ofType t = true
  | otherwise = false
